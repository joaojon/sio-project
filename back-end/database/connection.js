const mongoose = require('mongoose');
const URI =
	'mongodb+srv://userSIO:sio123@cluster0.fosxa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

const connectDB = async () => {
	await mongoose.connect(URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});

	console.log('Database connected');
};

module.exports = connectDB;

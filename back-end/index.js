const express = require('express');
const app = express();
const port = 3000;
const cors = require('cors');
const apiRouter = require('./api');
const connectDB = require('./database/connection');

connectDB();

app.use('/api', cors(), apiRouter);

app.use(function (err, req, res, next) {
	if (err.name === 'ValidationError') {
		res.status(400);
	} else {
		res.status(err.status || 500);
	}

	console.error(err);

	res.send({
		message: err.message,
	});
});

app.listen(port, () => {
	console.log(`Listening at http://localhost:${port}`);
});

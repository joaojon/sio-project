const fsp = require('fs').promises;
const xml2js = require('xml2js');
const Saft = require('../models/Saft');
const parser = new xml2js.Parser({ explicitArray: false, ignoreAttrs: true });

const parseJsonNumbers = function (json) {
	if (Array.isArray(json)) {
		for (let i = 0; i < json.length; i++) {
			let obj = json[i];
			for (let prop in obj) {
				if (typeof obj[prop] === 'object') {
					obj[prop] = parseJsonNumbers(obj[prop]);
				} else if (
					obj.hasOwnProperty(prop) &&
					obj[prop] !== null &&
					!isNaN(obj[prop])
				) {
					obj[prop] = +obj[prop];
				}
			}
		}
	} else {
		let obj = json;

		for (let prop in obj) {
			if (typeof obj[prop] === 'object') {
				obj[prop] = parseJsonNumbers(obj[prop]);
			} else if (
				obj.hasOwnProperty(prop) &&
				obj[prop] !== null &&
				!isNaN(obj[prop])
			) {
				obj[prop] = +obj[prop];
			}
		}
	}
	return json;
};

// Upload of SAF-T
const uploadSaft = async (req, res, next) => {
	if (!req.file) {
		next({
			message: 'Missing Saft',
			status: 400,
		});
	} else {
		const xmlSaft = await fsp.readFile(req.file.path, 'binary').catch(next);
		const json = await parser.parseStringPromise(xmlSaft).catch(next);

		if (json) {
			const jsonSaft = parseJsonNumbers(json);

			const version = json.AuditFile.Header.AuditFileVersion;
			if (
				!(
					version === '1.01_01' ||
					version === '1.02_01' ||
					version === '1.03_01' ||
					version === '1.04_01'
				)
			) {
				next({
					message: 'SAF-T version needs to be updated',
					status: 404,
				});
			}

			const saveSaft = {
				Header: jsonSaft.AuditFile.Header,
				MasterFiles: jsonSaft.AuditFile.MasterFiles || undefined,
				GeneralLedgerEntries:
					jsonSaft.AuditFile.GeneralLedgerEntries || undefined,
				SourceDocuments:
					jsonSaft.AuditFile.SourceDocuments || undefined,
			};

			await Saft.deleteOne({
				'Header.FiscalYear': saveSaft.Header.FiscalYear,
			}).catch(next);
			await Saft(saveSaft).save().catch(next);
			res.status(200).json(jsonSaft);
			await fsp.unlink(req.file.path);
		}
	}
};

module.exports = {
	uploadSaft,
};

const saft = require('../models/Saft');

function isArray(what) {
	return Object.prototype.toString.call(what) === '[object Array]';
}

const getFiscalYears = async (req, res, next) => {
	const found = await saft.find().catch(next);
	let fiscalYears = [];

	if (found.length > 0) {
		found.forEach((saft) => {
			fiscalYears.push(saft.Header.FiscalYear);
		});
		res.json(fiscalYears);
	} else {
		res.status(204).send('No data stored');
	}
};

const getJSON = async (req, res, next) => {
	const found = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.catch(next);

	if (found) {
		res.json(found);
	} else {
		res.status(204).send('No data stored');
	}
};

const getKPI = async (req, res, next) => {
	const purchasesTotal = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$SourceDocuments.PurchaseInvoices.Invoice' },
			{
				$group: {
					_id: null,
					GrossTotal: {
						$sum: '$SourceDocuments.PurchaseInvoices.Invoice.DocumentTotals.GrossTotal',
					},
					NetTotal: {
						$sum: '$SourceDocuments.PurchaseInvoices.Invoice.DocumentTotals.NetTotal',
					},
				},
			},
			{ $sort: { _id: 1 } },
		])
		.catch(next);

	const salesTotal = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$SourceDocuments.SalesInvoices.Invoice' },
			{
				$group: {
					_id: null,
					GrossTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.DocumentTotals.GrossTotal',
					},
					NetTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.DocumentTotals.NetTotal',
					},
				},
			},
			{ $sort: { _id: 1 } },
		])
		.catch(next);

	if (
		salesTotal &&
		salesTotal.length > 0 &&
		purchasesTotal &&
		purchasesTotal.length > 0
	) {
		purchasesTotal[0].GrossTotal = purchasesTotal[0].GrossTotal * -1;
		purchasesTotal[0].NetTotal = purchasesTotal[0].NetTotal * -1;
		delete purchasesTotal[0]._id;
		delete salesTotal[0]._id;
		res.json({
			TotalSales: salesTotal[0],
			TotalPurchases: purchasesTotal[0],
			TotalNetProfit: salesTotal[0].NetTotal - purchasesTotal[0].NetTotal,
		});
	} else {
		next({
			message: `No Info`,
			status: 404,
		});
	}
};

const getSales = async (req, res, next) => {
	const sales = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('SourceDocuments.SalesInvoices.Invoice')
		.catch(next);

	let allSales = [];

	const clients = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$MasterFiles.Customer' },
			{
				$project: {
					'MasterFiles.Customer.CustomerID': 1,
					'MasterFiles.Customer.CompanyName': 1,
					'MasterFiles.Customer.BillingAddress': 1,
				},
			},
			{ $sort: { _id: -1 } },
		])
		.catch(next);

	let customers = [];

	for (data in clients) {
		customers.push(clients[data].MasterFiles.Customer);
	}

	for (const invoice of sales[0].SourceDocuments.SalesInvoices.Invoice) {
		let invoiceObject = {
			id: invoice.InvoiceNo,
			date: invoice.InvoiceDate,
			customerID: invoice.CustomerID,
			customer: customers.find(
				({ CustomerID }) => CustomerID === invoice.CustomerID
			).CompanyName,
			total: invoice.DocumentTotals.GrossTotal,
			tax: invoice.DocumentTotals.TaxPayable,
			totalWithoutTax: invoice.DocumentTotals.NetTotal,
		};

		allSales.push(invoiceObject);
	}

	await res.json(allSales);
};

const getTopSales = async (req, res, next) => {
	const salesPerProduct = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: { path: '$SourceDocuments.SalesInvoices.Invoice' } },
			{
				$unwind: {
					path: '$SourceDocuments.SalesInvoices.Invoice.Line',
				},
			},
			{
				$unwind: {
					path: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
				},
			},
			{
				$group: {
					_id: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
					ProductPrice: {
						$addToSet:
							'$SourceDocuments.SalesInvoices.Invoice.Line.UnitPrice',
					},
					ProductDesc: {
						$addToSet:
							'$SourceDocuments.SalesInvoices.Invoice.Line.Description',
					},
					ProductTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.Line.CreditAmount',
					},
					ProductAverage: {
						$avg: '$SourceDocuments.SalesInvoices.Invoice.Line.CreditAmount',
					},
					ProductCode: {
						$avg: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
					},
					productCount: { $sum: 1 },
				},
			},
			{ $sort: { ProductTotal: -1 } },
		])
		.catch(next);

	let products = [];
	let productDesc;
	let productTotal;
	let productAverage;
	let productCount;
	let productPrice;

	for (i = 0; i < 5; i++) {
		productPrice = salesPerProduct[i].ProductPrice[0];
		productDesc = salesPerProduct[i].ProductDesc[0];
		productTotal = salesPerProduct[i].ProductTotal;
		productAverage = (
			Math.round(salesPerProduct[i].ProductAverage * 100) / 100
		).toFixed(2);
		productCount = salesPerProduct[i].productCount;
		products.push({
			productPrice,
			productDesc,
			productTotal,
			productAverage,
			productCount,
		});
	}

	await res.json(products);
};

const getTopSalesClient = async (req, res, next) => {
	const salesPerClient = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$SourceDocuments.SalesInvoices.Invoice' },
			{
				$group: {
					_id: '$SourceDocuments.SalesInvoices.Invoice.CustomerID',
					allSalesGrossTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.DocumentTotals.GrossTotal',
					},
					allSalesNetTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.DocumentTotals.NetTotal',
					},
				},
			},
			{ $sort: { allSalesNetTotal: -1 } },
		])
		.catch(next);

	let sales = [];

	const clients = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$MasterFiles.Customer' },
			{
				$project: {
					'MasterFiles.Customer.CustomerID': 1,
					'MasterFiles.Customer.CompanyName': 1,
					'MasterFiles.Customer.BillingAddress': 1,
				},
			},
			{ $sort: { _id: -1 } },
		])
		.catch(next);

	let customers = [];

	for (data in clients) {
		customers.push(clients[data].MasterFiles.Customer);
	}

	for (let i = 0; i < 5; i++) {
		sales.push({
			_id: salesPerClient[i]._id,
			allSalesGrossTotal: salesPerClient[i].allSalesGrossTotal,
			allSalesNetTotal: salesPerClient[i].allSalesNetTotal,
			CustomerID: customers.find(
				(customer) => customer.CustomerID === salesPerClient[i]._id
			).CompanyName,
		});
	}

	await res.json(sales);
};

const getSalesByCostumer = async (req, res, next) => {
	const sales = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('SourceDocuments.SalesInvoices.Invoice')
		.catch(next);

	let allSales = [];

	for (const invoice of sales[0].SourceDocuments.SalesInvoices.Invoice) {
		if (invoice.CustomerID === req.params.id) {
			let invoiceObject = {
				id: invoice.InvoiceNo,
				date: invoice.InvoiceDate,
				customerID: invoice.CustomerID,
				total: invoice.DocumentTotals.GrossTotal,
				tax: invoice.DocumentTotals.TaxPayable,
				totalWithoutTax: invoice.DocumentTotals.NetTotal,
			};
			allSales.push(invoiceObject);
		}
	}

	await res.json(allSales);
};

const getSalesLinesBySale = async (req, res, next) => {
	const sales = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('SourceDocuments.SalesInvoices.Invoice')
		.catch(next);

	let lines = [];

	for (const invoice of sales[0].SourceDocuments.SalesInvoices.Invoice) {
		if (invoice.InvoiceNo === req.params.id) {
			if (isArray(invoice.Line)) {
				invoice.Line.forEach((line) => {
					let invoiceLine = {
						ProductCode: line.ProductCode,
						ProductDescription: line.ProductDescription,
						Quantity: line.Quantity,
						UnitPrice: line.UnitPrice,
						NetTotal: line.CreditAmount,
						GrossTotal:
							line.CreditAmount +
							line.CreditAmount * (line.Tax.TaxPercentage / 100),
					};
					lines.push(invoiceLine);
				});
			} else {
				let invoiceLine = {
					ProductCode: invoice.Line.ProductCode,
					ProductDescription: invoice.Line.ProductDescription,
					Quantity: invoice.Line.Quantity,
					UnitPrice: invoice.Line.UnitPrice,
					NetTotal: invoice.Line.CreditAmount,
					GrossTotal:
						invoice.Line.CreditAmount +
						invoice.Line.CreditAmount *
							(invoice.Line.Tax.TaxPercentage / 100),
				};
				lines.push(invoiceLine);
			}
			await res.json(lines);
			next();
		}
	}
	if (lines.length == 0) res.status(400).send('Invalid invoice NO');
};

const getSalesInvoicesDiagram = async (req, res, next) => {
	const jsonInvoicesMonth = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$SourceDocuments.SalesInvoices.Invoice' },
			{
				$group: {
					_id: '$SourceDocuments.SalesInvoices.Invoice.Period',
					MonthTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.DocumentTotals.GrossTotal',
					},
					MonthTotalWithoutTax: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.DocumentTotals.NetTotal',
					},
				},
			},
			{ $sort: { _id: 1 } },
		])
		.catch(next);

	let months = [];
	let monthTotal = [];
	let monthTotalWithoutTax = [];

	for (data in jsonInvoicesMonth) {
		months.push('' + jsonInvoicesMonth[data]._id);
		monthTotal.push(jsonInvoicesMonth[data].MonthTotal);
		monthTotalWithoutTax.push(jsonInvoicesMonth[data].MonthTotalWithoutTax);
	}

	res.json({
		months,
		monthTotal,
		monthTotalWithoutTax,
	});
};

const getBuys = async (req, res, next) => {
	const buys = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('SourceDocuments.PurchaseInvoices.Invoice')
		.catch(next);

	let allBuys = [];

	const sup = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$MasterFiles.Supplier' },
			{
				$project: {
					'MasterFiles.Supplier.SupplierID': 1,
					'MasterFiles.Supplier.CompanyName': 1,
					'MasterFiles.Supplier.BillingAddress': 1,
				},
			},
			{ $sort: { _id: -1 } },
		])
		.catch(next);

	let suppliers = [];

	for (data in sup) {
		suppliers.push(sup[data].MasterFiles.Supplier);
	}

	for (const buy of buys[0].SourceDocuments.PurchaseInvoices.Invoice) {
		let buyObject = {
			id: buy.InvoiceNo,
			date: buy.InvoiceDate,
			supplierID: buy.SuplierID,
			supplier: suppliers.find(
				({ SupplierID }) => SupplierID === buy.SuplierID
			).CompanyName,
			total: buy.DocumentTotals.GrossTotal * -1,
			tax: buy.DocumentTotals.TaxPayable * -1,
			totalWithoutTax: buy.DocumentTotals.NetTotal * -1,
		};

		allBuys.push(buyObject);
	}

	await res.json(allBuys);
};

const getBuysBySupplier = async (req, res, next) => {
	const sales = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('SourceDocuments.PurchaseInvoices.Invoice')
		.catch(next);

	let allBuys = [];

	for (const invoice of sales[0].SourceDocuments.PurchaseInvoices.Invoice) {
		if (invoice.SuplierID === req.params.id) {
			let invoiceObject = {
				id: invoice.InvoiceNo,
				date: invoice.InvoiceDate,
				suplierID: invoice.SuplierID,
				total: invoice.DocumentTotals.GrossTotal * -1,
				tax: invoice.DocumentTotals.TaxPayable * -1,
				totalWithoutTax: invoice.DocumentTotals.NetTotal * -1,
			};
			allBuys.push(invoiceObject);
		}
	}
	if (allBuys.length > 0) await res.json(allBuys);
	else res.status(404).send('No buys for the given supplier');
};

const getBuysLinesByBuy = async (req, res, next) => {
	const buys = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('SourceDocuments.PurchaseInvoices.Invoice')
		.catch(next);

	let lines = [];

	for (const invoice of buys[0].SourceDocuments.PurchaseInvoices.Invoice) {
		if (invoice.InvoiceNo === req.params.id) {
			if (isArray(invoice.Line)) {
				invoice.Line.forEach((line) => {
					let invoiceLine = {
						ProductCode: line.ProductCode,
						ProductDescription: line.ProductDescription,
						Quantity: line.Quantity * -1,
						UnitPrice: line.UnitPrice * -1,
						NetTotal:
							line.DebitAmount * -1 -
							(line.DebitAmount * -1 * line.Tax.TaxPercentage) /
								100,
						GrossTotal: line.DebitAmount * -1,
					};
					lines.push(invoiceLine);
				});
			} else {
				let invoiceLine = {
					ProductCode: invoice.Line.ProductCode,
					ProductDescription: invoice.Line.ProductDescription,
					Quantity: invoice.Line.Quantity * -1,
					UnitPrice: invoice.Line.UnitPrice * -1,
					NetTotal:
						invoice.Line.DebitAmount * -1 -
						(invoice.Line.DebitAmount *
							-1 *
							invoice.Line.Tax.TaxPercentage) /
							100,
					GrossTotal: invoice.Line.DebitAmount * -1,
				};
				lines.push(invoiceLine);
			}
			await res.json(lines);
			next();
		}
	}
	if (lines.length == 0) res.status(400).send('Invalid invoice NO');
};

const getBuysInvoicesDiagram = async (req, res, next) => {
	const jsonInvoicesMonth = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('SourceDocuments.PurchaseInvoices.Invoice')
		.catch(next);

	let months = new Array(12);
	let monthTotal = new Array(12);
	let monthTotalWithoutTax = new Array(12);

	for (let i = 0; i < months.length; i++) {
		months[i] = i + 1;
		monthTotal[i] = 0;
		monthTotalWithoutTax[i] = 0;
	}

	jsonInvoicesMonth[0].SourceDocuments.PurchaseInvoices.Invoice.forEach(
		(invoice) => {
			if (
				parseInt(invoice.InvoiceDate.split('-')[1]) > 0 &&
				parseInt(invoice.InvoiceDate.split('-')[1]) < 12
			) {
				monthTotal[parseInt(invoice.InvoiceDate.split('-')[1])] +=
					invoice.DocumentTotals.GrossTotal * -1;
				monthTotalWithoutTax[
					parseInt(invoice.InvoiceDate.split('-')[1])
				] += invoice.DocumentTotals.NetTotal * -1;
			}
		}
	);

	res.json({
		months,
		monthTotal,
		monthTotalWithoutTax,
	});
};

const getCustomers = async (req, res, next) => {
	const clients = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$MasterFiles.Customer' },
			{
				$project: {
					'MasterFiles.Customer.CustomerID': 1,
					'MasterFiles.Customer.CompanyName': 1,
					'MasterFiles.Customer.BillingAddress': 1,
				},
			},
			{ $sort: { _id: -1 } },
		])
		.catch(next);

	let customers = [];

	for (data in clients) {
		customers.push(clients[data].MasterFiles.Customer);
	}

	const invoicesPerCustomer = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$SourceDocuments.SalesInvoices.Invoice' },
			{
				$group: {
					_id: '$SourceDocuments.SalesInvoices.Invoice.CustomerID',
					MonthTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.DocumentTotals.GrossTotal',
					},
				},
			},
			{ $sort: { _id: 1 } },
		])
		.catch(next);

	invoicesPerCustomer.forEach((invoice) => {
		customers.find(
			({ CustomerID }) => CustomerID === invoice._id
		).MonthTotal = invoice.MonthTotal;
	});

	res.json(customers);
};

const getTableProductSales = async (req, res, next) => {
	const salesPerProduct = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: { path: '$SourceDocuments.SalesInvoices.Invoice' } },
			{
				$unwind: {
					path: '$SourceDocuments.SalesInvoices.Invoice.Line',
				},
			},
			{
				$unwind: {
					path: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
				},
			},
			{
				$group: {
					_id: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
					ProductPrice: {
						$addToSet:
							'$SourceDocuments.SalesInvoices.Invoice.Line.UnitPrice',
					},
					ProductDesc: {
						$addToSet:
							'$SourceDocuments.SalesInvoices.Invoice.Line.Description',
					},
					ProductTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.Line.CreditAmount',
					},
					ProductAverage: {
						$avg: '$SourceDocuments.SalesInvoices.Invoice.Line.CreditAmount',
					},
					ProductCode: {
						$avg: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
					},
					productCount: { $sum: 1 },
				},
			},
			{ $sort: { ProductTotal: -1 } },
		])
		.catch(next);

	let products = [];
	let productDesc;
	let productTotal;
	let productAverage;
	let productCount;
	let productPrice;

	for (i in salesPerProduct) {
		productPrice = salesPerProduct[i].ProductPrice[0];
		productDesc = salesPerProduct[i].ProductDesc[0];
		productTotal = salesPerProduct[i].ProductTotal;
		productAverage = (
			Math.round(salesPerProduct[i].ProductAverage * 100) / 100
		).toFixed(2);
		productCount = salesPerProduct[i].productCount;
		products.push({
			productPrice,
			productDesc,
			productTotal,
			productAverage,
			productCount,
		});
	}

	res.json(products);
};

const getPieChartProductGroupSales = async (req, res, next) => {
	const salesPerProduct = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: { path: '$SourceDocuments.SalesInvoices.Invoice' } },
			{
				$unwind: {
					path: '$SourceDocuments.SalesInvoices.Invoice.Line',
				},
			},
			{
				$unwind: {
					path: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
				},
			},
			{
				$group: {
					_id: '$SourceDocuments.SalesInvoices.Invoice.Line.ProductCode',
					ProductTotal: {
						$sum: '$SourceDocuments.SalesInvoices.Invoice.Line.CreditAmount',
					},
					productCount: { $sum: 1 },
				},
			},
		])
		.catch(next);

	let products = [];
	let productTotal = [];
	let productCount = [];

	for (i in salesPerProduct) {
		products.push({ ID: salesPerProduct[i]._id });
		productTotal.push(salesPerProduct[i].ProductTotal);
		productCount.push(salesPerProduct[i].productCount);
	}

	const groups = await saft
		.aggregate([
			{ $match: { 'Header.FiscalYear': +req.params.fiscalYear } },
			{ $unwind: '$MasterFiles.Product' },
			{
				$group: {
					_id: '$MasterFiles.Product.ProductGroup',
					ProductCodes: {
						$addToSet: '$MasterFiles.Product.ProductCode',
					},
				},
			},
			{ $sort: { _id: 1 } },
		])
		.catch(next);

	let groupName = [];
	let groupTotal = [];
	let groupCount = [];
	let pindex, product;

	for (i in groups) {
		groupTotal[i] = 0;
		groupCount[i] = 0;
	}

	for (i in groups) {
		groupName.push(`${groups[i]._id}`);

		for (j in products) {
			product = products[j].ID;
			if (groups[i].ProductCodes.includes(product)) {
				pindex = products.findIndex((p) => p.ID === product);
				if (pindex != 1) {
					groupTotal[i] += productTotal[pindex];
					groupCount[i] += productCount[pindex];
				}
			}
		}
	}

	for (let i = 0; i < groupName.length; i++) {
		if (groupCount[i] === 0) {
			groupName.splice(i, 1);
			groupCount.splice(i, 1);
			groupTotal.splice(i, 1);
		}
	}

	res.json({
		groupName,
		groupTotal,
		groupCount,
	});
};

const getCompanyInfo = async (req, res, next) => {
	const companyInfo = await saft
		.find({ 'Header.FiscalYear': +req.params.fiscalYear })
		.select('Header')
		.catch(next);

	res.json(companyInfo[0].Header);
};

module.exports = {
	getFiscalYears,

	getJSON,
	getKPI,

	getSales,
	getTopSales,
	getTopSalesClient,
	getSalesByCostumer,
	getSalesLinesBySale,
	getSalesInvoicesDiagram,

	getBuys,
	getBuysBySupplier,
	getBuysLinesByBuy,
	getBuysInvoicesDiagram,

	getCustomers,

	getTableProductSales,
	getPieChartProductGroupSales,

	getCompanyInfo,
};

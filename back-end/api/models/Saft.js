var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
	Header: { type: Object, required: true },
	MasterFiles: { type: Object },
	GeneralLedgerEntries: { type: Object },
	SourceDocuments: { type: Object },
});

module.exports = mongoose.model('Saft', Schema, 'Saft');

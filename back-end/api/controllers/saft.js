var express = require('express');
var router = express.Router();
var saft = require('../services/saft');

router.get('/fiscal-years', saft.getFiscalYears);

router.get('/saft/:fiscalYear', saft.getJSON);
router.get('/kpi/:fiscalYear', saft.getKPI);

router.get('/sales/:fiscalYear', saft.getSales);
router.get('/top-sales-product/:fiscalYear', saft.getTopSales);
router.get('/top-sales-client/:fiscalYear', saft.getTopSalesClient);
router.get('/sales-by-costumer/:id/:fiscalYear', saft.getSalesByCostumer);
router.get('/sales-lines/:id/:fiscalYear', saft.getSalesLinesBySale);
router.get('/sales-invoices-diagram/:fiscalYear', saft.getSalesInvoicesDiagram);

router.get('/buys/:fiscalYear', saft.getBuys);
router.get('/buys-by-supplier/:id/:fiscalYear', saft.getBuysBySupplier);
router.get('/buys-lines/:id/:fiscalYear', saft.getBuysLinesByBuy);
router.get('/buys-invoices-diagram/:fiscalYear', saft.getBuysInvoicesDiagram);

router.get('/customers/:fiscalYear', saft.getCustomers);

router.get('/products/:fiscalYear', saft.getTableProductSales);
router.get('/productGroups/:fiscalYear', saft.getPieChartProductGroupSales);

router.get('/company/:fiscalYear', saft.getCompanyInfo);

module.exports = router;

const express = require('express');

const router = express.Router();
const multer = require('../../config/multer');
const parser = require('../services/parser');

router.post('/upload', multer.single('saft'), parser.uploadSaft);

module.exports = router;

const express = require('express');
const parser = require('./controllers/parser');
const saft = require('./controllers/saft');

const router = express.Router();

router.get('/', (res) => {
	res.json({ status: 'This is SIO' });
});

router.use(parser);

router.use(saft);

module.exports = router;

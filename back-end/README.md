# How to execute

On the back-end directory, run the following on your terminal:
```
npm install
node index.js
```

(To connect to the database, you need to request to the access to mongo atlas database)
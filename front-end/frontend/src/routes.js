import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import DefaultPage from "./pages/DefaultPage/index";

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={DefaultPage} />
      </Switch>
    </BrowserRouter>
  );
}

import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import { Cascader, Layout, Menu } from "antd";

import Dashboard from "../../components/Dashboard/index";
import Sales from "../../components/Sales/index";
import Purchases from "../../components/Purchases/index";
import Customers from "../../components/Customers/index";
import CustomerDetails from "../../components/CustomerDetails/index";
import SupplierDetails from "../../components/SupplierDetails/index";
import Product from "../../components/Product/index";
import SAFT from "../../components/SAFT/index";
import Company from "../../components/Company/index";
import FiscalYear from "../../components/FiscalYear/index";

import DomainIcon from "@material-ui/icons/Domain";

import "./style.css";

const DefaultPage = () => {
  const { Header, Content, Footer } = Layout;
  const [content, setContent] = useState(<Dashboard />);
  const [fiscalYearDisplay, setFiscalYearDisplay] = useState(null);

  useEffect(() => {
    const year = localStorage.getItem("fiscalYear");
    setFiscalYearDisplay(year);
    console.log(year);

    setContent(<FiscalYear />);
  }, []);

  function setHome() {
    const structure = <Dashboard />;
    setContent(structure);
  }

  function setSales() {
    const structure = (
      <Sales onCustomerDetails={setCustomerDetails.bind(this)} />
    );
    setContent(structure);
  }

  function setPurchases() {
    const structure = (
      <Purchases onSupplierDetails={setSupplierDetails.bind(this)} />
    );
    setContent(structure);
  }

  function setCustomers() {
    const structure = <Customers />;
    setContent(structure);
  }

  function setCustomerDetails(info) {
    const structure = <CustomerDetails customerID={info} />;
    setContent(structure);
  }

  function setSupplierDetails(info) {
    const structure = <SupplierDetails supplierID={info} />;
    setContent(structure);
  }

  function setProduct() {
    const structure = <Product />;
    setContent(structure);
  }

  function setSAFT() {
    const structure = <SAFT />;
    setContent(structure);
  }

  function setCompany() {
    const structure = <Company />;
    setContent(structure);
  }

  function setFiscalYear() {
    const structure = <FiscalYear />;
    setContent(structure);
  }

  return (
    <Layout className="layout">
      <Header>
        <div className="logo">
          <DomainIcon style={{ fill: "white", fontSize: "50" }} />
        </div>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["8"]}>
          <Menu.Item key="1" onClick={setHome}>
            Home
          </Menu.Item>
          <Menu.Item key="2" onClick={setSales}>
            Sales
          </Menu.Item>
          <Menu.Item key="3" onClick={setPurchases}>
            Purchases
          </Menu.Item>
          <Menu.Item key="4" onClick={setCustomers}>
            Customers
          </Menu.Item>
          <Menu.Item key="5" onClick={setProduct}>
            Products
          </Menu.Item>
          <Menu.Item key="6" onClick={setSAFT}>
            SAF-T
          </Menu.Item>
          <Menu.Item key="7" onClick={setCompany}>
            About
          </Menu.Item>
          <Menu.Item key="8" onClick={setFiscalYear}>
            Fiscal Year
          </Menu.Item>
        </Menu>
      </Header>
      {/* <Card title="Fiscal Year: 2020"></Card> */}
      <Content style={{ padding: "0 50px" }}>{content}</Content>
      <Footer style={{ textAlign: "center" }}>Amarante Shopping</Footer>
    </Layout>
  );
};

export default DefaultPage;

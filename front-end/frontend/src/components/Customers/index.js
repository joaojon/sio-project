import React, { useEffect, useState } from "react";
import { Card, Table } from "antd";
import { Pie } from "@ant-design/charts";

import api from "../../services/api";

import "./style.css";

const Customers = (props) => {
  const [dataSource, setDataSource] = useState([]);
  const [dataSourceCostumersValue, setDataSourceCostumersValue] = useState([]);
  const [dataCustomersCountry, setDataCustomersCountry] = useState([]);
  const [filtersTaxId, setFiltersTaxId] = useState([]);
  const [filtersName, setFiltersName] = useState([]);
  const [fiscalYear, setFiscalYear] = useState(null);

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");
      setFiscalYear(year);

      if (year !== null) {
        const response = await api.get(`/api/customers/${year}`);

        console.log(response.data);

        let customers = [];
        let customersCountry = {};
        let customersValue = [];
        let chartDataArray = [];

        const customerID = [];
        const filterCustomerID = [];
        const customerName = [];
        const filterCustomerName = [];

        response.data.map((item, index) => {
          customers.push({
            key: index,
            customerID: item.CustomerID,
            name: item.CompanyName,
            address: `${item.BillingAddress.AddressDetail}, ${item.BillingAddress.City}, ${item.BillingAddress.Country}`,
            total: item.MonthTotal
              ? new Intl.NumberFormat("pt-PT", {
                  style: "currency",
                  currency: "EUR",
                }).format(item.MonthTotal)
              : `${0} €`,
          });

          const country = item.BillingAddress.Country;
          if (!(item.BillingAddress.Country in customersCountry)) {
            customersCountry[country] = 1;
          } else {
            customersCountry[country]++;
          }

          customersValue.push({
            type: item.CompanyName,
            value: item.MonthTotal,
          });

          if (!customerID.includes(item.CustomerID)) {
            customerID.push(item.CustomerID);
          }
          if (!customerName.includes(item.CompanyName)) {
            customerName.push(item.CompanyName);
          }
        });

        customerID.map((item) => {
          filterCustomerID.push({
            text: item,
            value: item,
          });
        });

        customerName.map((item) => {
          filterCustomerName.push({
            text: item,
            value: item,
          });
        });

        for (const field in customersCountry) {
          chartDataArray.push({
            type: field,
            value: customersCountry[field],
          });
        }

        setDataSource(customers);
        setDataSourceCostumersValue(customersValue);
        setDataCustomersCountry(chartDataArray);
        setFiltersTaxId(filterCustomerID);
        setFiltersName(filterCustomerName);
      }
    })();
  }, []);

  const columns = [
    {
      title: "Customer ID",
      dataIndex: "customerID",
      key: "customerID",
      filters: filtersTaxId,
      onFilter: (value, record) => record.customerID.indexOf(value) === 0,
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      filters: filtersName,
      onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ["descend"],
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      align: "right",
      title: "Total",
      dataIndex: "total",
      key: "total",
      sorter: (a, b) =>
        parseFloat(a.total.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.total.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
  ];

  var configCustomersCountry = {
    appendPadding: 10,
    data: dataCustomersCountry,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "spider",
      labelHeight: 28,
      content: "{name}",
    },
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
  };

  var configCustomersValue = {
    appendPadding: 10,
    data: dataSourceCostumersValue,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "spider",
      labelHeight: 28,
      content: "{name}",
    },
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
  };

  return (
    <div className="customersContainer">
      <div className="fiscalYearContainer">
        <h2>Fiscal Year {fiscalYear}</h2>
      </div>
      <div className="chartsCustomersContainer">
        <Card title="Costumers per Country" style={{ width: "45%" }}>
          {fiscalYear === null ? (
            <h1>No Data</h1>
          ) : (
            <Pie {...configCustomersCountry} />
          )}
        </Card>
        <Card title="Total Sales per Customer" style={{ width: "45%" }}>
          {fiscalYear === null ? (
            <h1>No Data</h1>
          ) : (
            <Pie {...configCustomersValue} />
          )}
        </Card>
      </div>
      <div className="customersTableContainer">
        <Table
          title={() => <h1>Customers</h1>}
          dataSource={dataSource}
          columns={columns}
          bordered
          pagination={{ pageSize: 20 }}
          scroll={{ y: 240 }}
        />
      </div>
    </div>
  );
};

export default Customers;

import React, { useEffect, useState } from "react";
import { Button, Card, DatePicker, Modal, Space, Table } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import moment from "moment";

import api from "../../services/api";

import "./style.css";

const Purchases = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [invoiceID, setInvoiceID] = useState("");

  const { RangePicker } = DatePicker;
  const [dataSource, setDataSource] = useState([]);
  const [dataPeriodTime, setDataPeriodTime] = useState([]);
  const [dataSourceLines, setDataSourceLines] = useState([]);
  const [allPurchases, setAllPurchases] = useState([]);
  const [initialDate, setInitialDate] = useState([]);
  const [finalDate, setFinalDate] = useState([]);
  const [filtersTaxId, setFiltersTaxId] = useState([]);
  const [fiscalYear, setFiscalYear] = useState(0);

  const showModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");
      setFiscalYear(year);

      if (year !== null) {
        const response = await api.get(`/api/buys/${year}`);

        const purchases = [];
        const supplierName = [];
        const filterName = [];
        const supplierID = [];
        const filterID = [];

        response.data.map((item, index) => {
          purchases.push({
            key: index,
            invoiceId: item.id,
            date: moment(item.date).format("DD/MM/YYYY"),
            supplierID: item.supplierID,
            supplierName: item.supplier,
            totalWithoutTax: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.totalWithoutTax),
            tax: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.tax),
            total: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.total),
          });

          if (!supplierName.includes(item.supplier)) {
            supplierName.push(item.supplier);
          }

          if (!supplierID.includes(item.supplierID)) {
            supplierID.push(item.supplierID);
          }
        });

        supplierName.map((item) => {
          filterName.push({
            text: item,
            value: item,
          });
        });

        supplierID.map((item) => {
          filterID.push({
            text: item,
            value: item,
          });
        });

        setDataSource(purchases);
        setAllPurchases(response.data);
        setFiltersTaxId(filterID);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");

      if (year !== null) {
        const invoiceIdBack = invoiceID.replace(" ", "%20");
        const invoiceIdComplete = invoiceIdBack.replace("/", "%2F");
        const response = await api.get(
          `/api/buys-lines/${invoiceIdComplete}/${year}`
        );

        console.log(response.data);

        const lines = [];

        response.data.map((item) => {
          lines.push({
            productCode: item.ProductCode,
            productDescription: item.ProductDescription,
            quantity: item.Quantity,
            unitPrice: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.UnitPrice),
            netTotal: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.NetTotal),
            grossTotal: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.GrossTotal),
          });
        });

        setDataSourceLines(lines);
      }
    })();
  }, [invoiceID]);

  function onChange(value, dateString) {
    console.log("Selected Time: ", value);
    console.log("Formatted Selected Time: ", dateString);

    setInitialDate(dateString[0]);
    setFinalDate(dateString[1]);
  }

  function loadSales() {
    let purchases = [];
    console.log(initialDate);
    console.log(finalDate);

    allPurchases.map((item, index) => {
      if (moment(item.date).isBetween(initialDate, finalDate)) {
        purchases.push({
          key: index,
          invoiceId: item.id,
          date: moment(item.date).format("DD/MM/YYYY"),
          supplierID: item.supplierID,
          supplierName: item.supplier,
          totalWithoutTax: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.totalWithoutTax),
          tax: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.tax),
          total: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.total),
        });
      }
    });

    setDataPeriodTime(purchases);
  }

  function detailsPage(supplierID) {
    props.onSupplierDetails(supplierID);
  }

  const columns = [
    {
      title: "Invoice ID",
      dataIndex: "invoiceId",
      key: "invoiceId",
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
    },
    {
      title: "Supplier ID",
      dataIndex: "supplierID",
      key: "supplierID",
      filters: filtersTaxId,
      onFilter: (value, record) => record.supplierID.indexOf(value) === 0,
    },
    {
      title: "Supplier",
      dataIndex: "supplierName",
      key: "supplierName",
      filters: filtersTaxId,
      onFilter: (value, record) => record.supplierName.indexOf(value) === 0,
    },
    {
      align: "right",
      title: "Total Net Amount",
      dataIndex: "totalWithoutTax",
      key: "totalWithoutTax",
      sorter: (a, b) =>
        parseFloat(
          a.totalWithoutTax.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseFloat(
          b.totalWithoutTax.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Tax Value",
      dataIndex: "tax",
      key: "tax",
      sorter: (a, b) =>
        parseFloat(a.tax.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.tax.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Total Gross Amount",
      dataIndex: "total",
      key: "total",
      sorter: (a, b) =>
        parseFloat(a.total.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.total.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "center",
      title: "",
      dataIndex: "",
      key: "invoiceDetails",
      render: (_, record) => (
        <Button
          type="primary"
          onClick={() => {
            setInvoiceID(record.invoiceId);
            showModal();
          }}
        >
          Invoice Details
        </Button>
      ),
    },
    {
      align: "center",
      title: "",
      dataIndex: "",
      key: "customerDetails",
      render: (_, record) => (
        <Button
          type="primary"
          onClick={() => {
            console.log(record.supplierID);
            detailsPage(record.supplierID);
          }}
        >
          Supplier Details
        </Button>
      ),
    },
  ];

  const columnsLines = [
    {
      title: "Product Code",
      dataIndex: "productCode",
      key: "productCode",
    },
    {
      title: "Product Description",
      dataIndex: "productDescription",
      key: "productDescription",
    },
    {
      align: "right",
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      align: "right",
      title: "Unit Price",
      dataIndex: "unitPrice",
      key: "unitPrice",
      sorter: (a, b) =>
        parseInt(a.unitPrice.split(" ")[0]) -
        parseInt(b.unitPrice.split(" ")[0]),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Net Total",
      dataIndex: "netTotal",
      key: "netTotal",
      sorter: (a, b) =>
        parseInt(a.netTotal.split(" ")[0]) - parseInt(b.netTotal.split(" ")[0]),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Gross Total",
      dataIndex: "grossTotal",
      key: "grossTotal",
      sorter: (a, b) =>
        parseInt(a.grossTotal.split(" ")[0]) -
        parseInt(b.grossTotal.split(" ")[0]),
      sortDirections: ["descend", "ascend"],
    },
  ];

  return (
    <div className="salesContainer">
      <div className="fiscalYearContainer">
        <h2>Fiscal Year {fiscalYear}</h2>
      </div>
      <div className="salesPeriodTime">
        <div className="inputsContainer">
          <Card title="Purchases in a Period of Time" className="cardInputs">
            <h3>Select the dates</h3>
            <Space direction="vertical" className="inputsSpace">
              <RangePicker onChange={onChange} />
            </Space>
            <Button
              className="button"
              type="primary"
              icon={<SearchOutlined />}
              onClick={loadSales}
            >
              Search
            </Button>
          </Card>
        </div>
        <div className="tablePeriodContainer">
          <Table
            title={() => <h1>Purchases</h1>}
            dataSource={dataPeriodTime}
            columns={columns}
            bordered
            pagination={{ pageSize: 20 }}
            scroll={{ y: 240 }}
            className="salesPeriodTable"
          />
        </div>
      </div>
      <div className="allSales">
        <Table
          title={() => <h1>All Purchases</h1>}
          dataSource={dataSource}
          columns={columns}
          bordered
          pagination={{ pageSize: 20 }}
          scroll={{ y: 240 }}
        />
      </div>
      <Modal
        title={`Invoice ${invoiceID}`}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1200}
        style={{ top: 20 }}
      >
        <div>
          <Table
            title={() => <h1>Invoice Lines</h1>}
            dataSource={dataSourceLines}
            columns={columnsLines}
            bordered
            pagination={{ pageSize: 20 }}
            scroll={{ y: 240 }}
          ></Table>
        </div>
      </Modal>
    </div>
  );
};

export default Purchases;

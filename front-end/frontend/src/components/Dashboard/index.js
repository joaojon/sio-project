import React, { useEffect, useState } from "react";
import { Card, Col, Row, Table } from "antd";
import { Line, Pie } from "@ant-design/charts";

import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";

import api from "../../services/api";

import "./style.css";

const Dashboard = (props) => {
  const [data, setData] = useState([]);
  const [dataPurchases, setDataPurchases] = useState([]);
  const [dataProfit, setDataProfit] = useState([]);

  const [totalGrossPurchases, setTotalGrossPurchases] = useState(0);
  const [totalNetPurchases, setTotalNetPurchases] = useState(0);
  const [totalGrossSales, setTotalGrossSales] = useState(0);
  const [totalNetSales, setTotalNetSales] = useState(0);
  const [totalNetProfit, setTotalNetProfit] = useState(0);

  const [dataSource, setDataSource] = useState([]);

  const [dataClientsNet, setDataClientsNet] = useState([]);
  //const [dataClientGross, setDataClientGross] = useState([]);
  const [dataTopProducts, setDataTopProducts] = useState([]);

  const [fiscalYear, setFiscalYear] = useState(0);

  const [topProductsContainer, setTopProductsContainer] = useState(null);
  const [topClientsContainer, setTopClientsContainer] = useState(null);
  const [salesChartContainer, setSalesChartContainer] = useState(null);
  const [purchasesChartContainer, setPurchasesChartContainer] = useState(null);
  const [profitChartContainer, setProfitChartContainer] = useState(null);

  const TotalNetSalesTitle = (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <MonetizationOnIcon size="large" />
      <h3 style={{ marginTop: "8px", marginLeft: "5px" }}>Total Net Sales</h3>
    </div>
  );

  const TotalGrossSalesTitle = (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <MonetizationOnIcon size="large" />
      <h3 style={{ marginTop: "8px", marginLeft: "5px" }}>Total Gross Sales</h3>
    </div>
  );

  const totalNetPurchasesTitle = (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <MonetizationOnIcon size="large" />
      <h3 style={{ marginTop: "8px", marginLeft: "5px" }}>
        Total Net Purchases
      </h3>
    </div>
  );

  const totalGrossPurchasesTitle = (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <MonetizationOnIcon size="large" />
      <h3 style={{ marginTop: "8px", marginLeft: "5px" }}>
        Total Gross Purchases
      </h3>
    </div>
  );

  const totalProfitTitle = (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <TrendingUpIcon size="large" />
      <h3 style={{ marginTop: "8px", marginLeft: "5px" }}>Gross Margin</h3>
    </div>
  );

  useEffect(() => {
    setFiscalYear(localStorage.getItem("fiscalYear"));
  }, []);

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");

      if (year !== null) {
        const response = await api.get(`/api/kpi/${fiscalYear}`);
        const response2 = await api.get(
          `/api/sales-invoices-diagram/${fiscalYear}`
        );
        const response3 = await api.get(
          `/api/buys-invoices-diagram/${fiscalYear}`
        );
        const response4 = await api.get(`/api/top-sales-product/${fiscalYear}`);
        const response5 = await api.get(`/api/top-sales-client/${fiscalYear}`);

        console.log(response5.data);

        const invoices = [];
        const invoicesPurchases = [];
        const invoicesProfit = [];

        const topProducts = [];

        const topClientsNet = [];
        const topClientsGross = [];
        const topProductsPie = [];

        response4.data.map((item, index) => {
          topProducts.push({
            key: index,
            productDescription: item.productDesc,
            quantitySold: item.productCount,
            unitPrice: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.productPrice),
            averageAmount: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.productAverage),
            totalAmountSold: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.productTotal),
          });
          topProductsPie.push({
            type: item.productDesc,
            value: item.productTotal,
          });
        });

        response5.data.map((item) => {
          topClientsNet.push({
            type: item.CustomerID,
            value: item.allSalesNetTotal,
          });

          topClientsGross.push({
            type: item.CustomerID,
            value: item.allSalesGrossTotal,
          });
        });

        let profit = {
          sales: [],
          purchases: [],
          profit: [],
          months: [],
        };

        response2.data.months.map((item, index) => {
          let monthString = "";
          switch (item) {
            case "1":
              monthString = "January";
              break;
            case "2":
              monthString = "February";
              break;
            case "3":
              monthString = "March";
              break;
            case "4":
              monthString = "April";
              break;
            case "5":
              monthString = "May";
              break;
            case "6":
              monthString = "June";
              break;
            case "7":
              monthString = "July";
              break;
            case "8":
              monthString = "August";
              break;
            case "9":
              monthString = "September";
              break;
            case "10":
              monthString = "October";
              break;
            case "11":
              monthString = "November";
              break;
            case "12":
              monthString = "December";
              break;
            default:
              break;
          }
          invoices.push({
            month: monthString,
            value: response2.data.monthTotal[index],
            category: "Gross Value",
          });
          invoices.push({
            month: monthString,
            value: response2.data.monthTotalWithoutTax[index],
            category: "Net Value",
          });
          invoicesPurchases.push({
            month: monthString,
            value: response3.data.monthTotal[index],
            category: "Gross Value",
          });
          invoicesPurchases.push({
            month: monthString,
            value: response3.data.monthTotalWithoutTax[index],
            category: "Net Value",
          });
          profit.sales.push(response2.data.monthTotalWithoutTax[index]);
          profit.purchases.push(response3.data.monthTotal[index]);
          profit.profit.push(
            response2.data.monthTotalWithoutTax[index] -
              response3.data.monthTotal[index]
          );
          profit.months.push(monthString);
        });

        profit.profit.map((item, index) => {
          invoicesProfit.push({
            month: profit.months[index],
            value: item,
            category: "Profit",
          });
          invoicesProfit.push({
            month: profit.months[index],
            value: profit.purchases[index],
            category: "Purchases",
          });
          invoicesProfit.push({
            month: profit.months[index],
            value: profit.sales[index],
            category: "Sales",
          });
        });

        setData(invoices);
        setDataPurchases(invoicesPurchases);
        setDataProfit(invoicesProfit);

        setTotalNetPurchases(
          new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(response.data.TotalPurchases.NetTotal)
        );
        setTotalGrossPurchases(
          new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(response.data.TotalPurchases.GrossTotal)
        );
        setTotalNetSales(
          new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(response.data.TotalSales.NetTotal)
        );
        setTotalGrossSales(
          new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(response.data.TotalSales.GrossTotal)
        );
        setTotalNetProfit(
          new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(response.data.TotalNetProfit)
        );
        setDataSource(topProducts);
        setDataClientsNet(topClientsNet);
        setDataTopProducts(topProductsPie);
      }
    })();
  }, [fiscalYear]);

  const config = {
    data,
    height: 400,
    xField: "month",
    yField: "value",
    seriesField: "category",
    point: {
      size: 7,
      shape: "diamond",
    },
  };

  const configPurchases = {
    data: dataPurchases,
    height: 400,
    xField: "month",
    yField: "value",
    seriesField: "category",
    point: {
      size: 7,
      shape: "diamond",
    },
  };

  const configProfit = {
    data: dataProfit,
    height: 400,
    xField: "month",
    yField: "value",
    seriesField: "category",
    point: {
      size: 7,
      shape: "diamond",
    },
  };

  const columns = [
    {
      title: "Product Description",
      dataIndex: "productDescription",
      key: "productDescription",
    },
    {
      align: "right",
      title: "Quantity Sold",
      dataIndex: "quantitySold",
      key: "quantitySold",
    },
    {
      align: "right",
      title: "Unit Price",
      dataIndex: "unitPrice",
      key: "unitPrice",
      sorter: (a, b) =>
        parseFloat(
          a.unitPrice.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseFloat(
          b.unitPrice.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Average Amount",
      dataIndex: "averageAmount",
      key: "averageAmount",
      sorter: (a, b) =>
        parseInt(
          a.averageAmount.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseInt(
          b.averageAmount.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["ascend", "descend"],
    },
    {
      align: "right",
      title: "Total Amount Sold",
      dataIndex: "totalAmountSold",
      key: "totalAmountSold",
      sorter: (a, b) =>
        parseInt(
          a.totalAmountSold.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseInt(
          b.totalAmountSold.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["descend", "ascend"],
    },
  ];

  var configNet = {
    appendPadding: 10,
    data: dataClientsNet,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "spider",
      labelHeight: 28,
      content: "{name}",
    },
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
  };

  var configProduct = {
    appendPadding: 10,
    data: dataTopProducts,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "spider",
      labelHeight: 28,
      content: "{name}",
    },
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
  };

  return (
    <div className="container">
      <div className="fiscalYearContainer">
        <h2>Fiscal Year {fiscalYear}</h2>
      </div>
      <Row gutter={16}>
        <Col span={8} className="columnCards">
          <Card title={totalNetPurchasesTitle} className="card">
            <h1>{fiscalYear === null ? "No Data" : totalNetPurchases}</h1>
          </Card>
          <Card title={totalGrossPurchasesTitle} className="card">
            <h1>{fiscalYear === null ? "No Data" : totalGrossPurchases}</h1>
          </Card>
        </Col>
        <Col span={8} className="columnCards">
          <Card title={totalProfitTitle} className="card">
            <h1>{fiscalYear === null ? "No Data" : totalNetProfit}</h1>
          </Card>
        </Col>
        <Col span={8} className="columnCards">
          <Card title={TotalNetSalesTitle} className="card">
            <h1>{fiscalYear === null ? "No Data" : totalNetSales}</h1>
          </Card>
          <Card title={TotalGrossSalesTitle} className="card">
            <h1>{fiscalYear === null ? "No Data" : totalGrossSales}</h1>
          </Card>
        </Col>
      </Row>
      <div className="tableProductsContainer">
        <Table
          title={() => <h1>Top 5 Products</h1>}
          dataSource={dataSource}
          columns={columns}
          bordered
          scroll={{ y: 300 }}
          style={{ width: 1350 }}
        />
      </div>
      <div className="pieChartsContainer">
        <Card title="Top 5 Clients Net Amount" style={{ width: "45%" }}>
          {fiscalYear === null ? topClientsContainer : <Pie {...configNet} />}
        </Card>
        <Card title="Top 5 Clients Gross Amount" style={{ width: "45%" }}>
          {fiscalYear === null ? (
            topProductsContainer
          ) : (
            <Pie {...configProduct} />
          )}
        </Card>
      </div>
      <div className="chartContainer">
        <Card
          hoverable
          title="Total Sales Amount per Month"
          style={{ width: 1360 }}
        >
          {fiscalYear === null ? salesChartContainer : <Line {...config} />}
        </Card>
      </div>
      <div className="chartContainer">
        <Card
          hoverable
          title="Total Purchases Amount per Month"
          style={{ width: 1360 }}
        >
          {fiscalYear === null ? (
            purchasesChartContainer
          ) : (
            <Line {...configPurchases} />
          )}
        </Card>
      </div>
      <div className="chartContainer">
        <Card
          hoverable
          title="Total Profit Amount of Invoices per Month"
          style={{ width: 1360 }}
        >
          {fiscalYear === null ? (
            profitChartContainer
          ) : (
            <Line {...configProfit} />
          )}
        </Card>
      </div>
    </div>
  );
};

export default Dashboard;

import React, { useEffect, useState } from "react";
import { Button, Card, DatePicker, Modal, Space, Table } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import moment from "moment";

import api from "../../services/api";

import "./style.css";

const Sales = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [invoiceID, setInvoiceID] = useState("");

  const { RangePicker } = DatePicker;
  const [dataSource, setDataSource] = useState([]);
  const [dataPeriodTime, setDataPeriodTime] = useState([]);
  const [dataSourceLines, setDataSourceLines] = useState([]);
  const [allSales, setAllSales] = useState([]);
  const [initialDate, setInitialDate] = useState([]);
  const [finalDate, setFinalDate] = useState([]);
  const [filtersNames, setFiltersNames] = useState([]);
  const [filtersTaxId, setFiltersTaxId] = useState([]);
  const [fiscalYear, setFiscalYear] = useState(0);

  const showModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");
      setFiscalYear(year);

      if (year !== null) {
        const response = await api.get(`/api/sales/${year}`);

        console.log(response.data);

        const sales = [];
        const customerNames = [];
        const customerID = [];
        const filterNames = [];
        const filterID = [];

        response.data.map((item, index) => {
          sales.push({
            key: index,
            invoiceId: item.id,
            date: moment(item.date).format("DD/MM/YYYY"),
            customerTaxId: item.customerID,
            costumer: item.customer,
            totalWithoutTax: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.totalWithoutTax),
            tax: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.tax),
            total: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.total),
          });

          if (!customerNames.includes(item.customer)) {
            customerNames.push(item.customer);
          }

          if (!customerID.includes(item.customerID)) {
            customerID.push(item.customerID);
          }
        });

        customerNames.map((item) => {
          filterNames.push({
            text: item,
            value: item,
          });
        });

        customerID.map((item) => {
          filterID.push({
            text: item,
            value: item,
          });
        });

        setDataSource(sales);
        setAllSales(response.data);
        setFiltersNames(filterNames);
        setFiltersTaxId(filterID);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");

      if (year !== null) {
        const invoiceIdBack = invoiceID.replace(" ", "%20");
        const invoiceIdComplete = invoiceIdBack.replace("/", "%2F");
        const response = await api.get(
          `/api/sales-lines/${invoiceIdComplete}/${year}`
        );

        console.log(response.data);

        const lines = [];

        response.data.map((item) => {
          lines.push({
            productCode: item.ProductCode,
            productDescription: item.ProductDescription,
            quantity: item.Quantity,
            unitPrice: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.UnitPrice),
            netTotal: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.NetTotal),
            grossTotal: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.GrossTotal),
          });
        });

        setDataSourceLines(lines);
      }
    })();
  }, [invoiceID]);

  function onChange(value, dateString) {
    // console.log("Selected Time: ", value);
    // console.log("Formatted Selected Time: ", dateString);

    setInitialDate(dateString[0]);
    setFinalDate(dateString[1]);
  }

  function loadSales() {
    let sales = [];
    // console.log(initialDate);
    // console.log(finalDate);

    allSales.map((item, index) => {
      if (moment(item.date).isBetween(initialDate, finalDate)) {
        sales.push({
          key: index,
          invoiceId: item.id,
          date: moment(item.date).format("DD/MM/YYYY"),
          customerTaxId: item.customerID,
          costumer: item.customer,
          totalWithoutTax: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.totalWithoutTax),
          tax: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.tax),
          total: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.total),
        });
      }
    });

    setDataPeriodTime(sales);
  }

  function seeData() {
    console.log(dataPeriodTime);
  }

  function detailsPage(customerID) {
    props.onCustomerDetails(customerID);
  }

  const columns = [
    {
      title: "Invoice ID",
      dataIndex: "invoiceId",
      key: "invoiceId",
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
    },
    {
      title: "Customer Tax ID",
      dataIndex: "customerTaxId",
      key: "customerTaxId",
      filters: filtersTaxId,
      onFilter: (value, record) => record.customerTaxId.indexOf(value) === 0,
    },
    {
      title: "Customer",
      dataIndex: "costumer",
      key: "costumer",
      filters: filtersNames,
      onFilter: (value, record) => record.costumer.indexOf(value) === 0,
    },
    {
      align: "right",
      title: "Total Net Amount",
      dataIndex: "totalWithoutTax",
      key: "totalWithoutTax",
      sorter: (a, b) =>
        parseFloat(
          a.totalWithoutTax.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseFloat(
          b.totalWithoutTax.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Tax",
      dataIndex: "tax",
      key: "tax",
      sorter: (a, b) =>
        parseFloat(a.tax.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.tax.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Total Gross Amount",
      dataIndex: "total",
      key: "total",
      sorter: (a, b) =>
        parseFloat(a.total.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.total.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "center",
      title: "",
      dataIndex: "",
      key: "invoiceDetails",
      render: (_, record) => (
        <Button
          type="primary"
          onClick={() => {
            setInvoiceID(record.invoiceId);
            showModal();
          }}
        >
          Invoice Details
        </Button>
      ),
    },
    {
      align: "center",
      title: "",
      dataIndex: "",
      key: "customerDetails",
      render: (_, record) => (
        <Button
          type="primary"
          onClick={() => {
            console.log(record.customerTaxId);
            detailsPage(record.customerTaxId);
          }}
        >
          Customer Details
        </Button>
      ),
    },
  ];

  const columnsLines = [
    {
      title: "Product Code",
      dataIndex: "productCode",
      key: "productCode",
    },
    {
      title: "Product Description",
      dataIndex: "productDescription",
      key: "productDescription",
    },
    {
      align: "right",
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      align: "right",
      title: "Unit Price",
      dataIndex: "unitPrice",
      key: "unitPrice",
      sorter: (a, b) =>
        parseFloat(
          a.unitPrice.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseFloat(
          b.unitPrice.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Net Total",
      dataIndex: "netTotal",
      key: "netTotal",
      sorter: (a, b) =>
        parseFloat(
          a.netTotal.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseFloat(b.netTotal.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Gross Total",
      dataIndex: "grossTotal",
      key: "grossTotal",
      sorter: (a, b) =>
        parseFloat(
          a.grossTotal.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseFloat(
          b.grossTotal.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["descend", "ascend"],
    },
  ];

  return (
    <div className="salesContainer">
      <div className="fiscalYearContainer">
        <h2>Fiscal Year {fiscalYear}</h2>
      </div>
      <div className="salesPeriodTime">
        <div className="inputsContainer">
          <Card
            title="Sales in a Period of Time"
            style={{ height: 250, width: "90%" }}
          >
            <h3>Select the dates</h3>
            <Space direction="vertical" className="inputsSpace">
              <RangePicker onChange={onChange} />
            </Space>
            <Button
              className="button"
              type="primary"
              icon={<SearchOutlined />}
              onClick={loadSales}
            >
              Search
            </Button>
          </Card>
        </div>
        <div className="tablePeriodContainer">
          <Table
            title={() => <h1>Sales</h1>}
            dataSource={dataPeriodTime}
            columns={columns}
            bordered
            pagination={{ pageSize: 20 }}
            scroll={{ y: 240 }}
            className="salesPeriodTable"
          />
        </div>
      </div>
      <div className="allSales" onClick={seeData}>
        <Table
          title={() => <h1>All Sales</h1>}
          dataSource={dataSource}
          columns={columns}
          bordered
          pagination={{ pageSize: 20 }}
          scroll={{ y: 240 }}
        />
      </div>
      <Modal
        title={`Invoice ${invoiceID}`}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1200}
        style={{ top: 20 }}
      >
        <div>
          <Table
            title={() => <h1>Invoice Lines</h1>}
            dataSource={dataSourceLines}
            columns={columnsLines}
            bordered
            pagination={{ pageSize: 20 }}
            scroll={{ y: 240 }}
          ></Table>
        </div>
      </Modal>
    </div>
  );
};

export default Sales;

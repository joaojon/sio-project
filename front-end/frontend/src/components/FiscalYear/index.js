import React, { useEffect, useState } from "react";
import { Card, Cascader } from "antd";

import "./style.css";

import api from "../../services/api";

const FiscalYear = ({ onSelected }) => {
  const [options, setOptions] = useState([]);

  useEffect(() => {
    (async () => {
      const response = await api.get(`/api/fiscal-years`);

      const fiscalYearOptions = [];

      response.data.map((item) => {
        fiscalYearOptions.push({
          value: item,
          label: item,
        });
      });

      setOptions(fiscalYearOptions);
    })();
  }, []);

  function onChange(value) {
    localStorage.setItem("fiscalYear", value);
  }

  return (
    <div className="fiscalYearContainerPage">
      <Card title="Select the fiscal Year" style={{ width: "45%" }}>
        <Cascader
          options={options}
          onChange={onChange}
          placeholder="Fiscal Year"
        />
      </Card>
    </div>
  );
};

export default FiscalYear;

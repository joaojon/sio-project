import React, { useEffect, useState } from "react";
import { Card, Table } from "antd";
import { Pie } from "@ant-design/charts";

import api from "../../services/api";

import "./style.css";

const Product = () => {
  const [dataSource, setDataSource] = useState([]);
  const [dataAmount, setDataAmount] = useState([]);
  const [dataSales, setDataSales] = useState([]);
  const [fiscalYear, setFiscalYear] = useState(null);

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");
      setFiscalYear(year);

      if (year !== null) {
        const response = await api.get(`/api/products/${year}`);
        const response2 = await api.get(`/api/productgroups/${year}`);

        console.log(response2.data);

        let products = [];
        let productAmountChart = [];
        let productSalesChart = [];

        response.data.map((item, index) => {
          products.push({
            key: index,
            description: item.productDesc,
            sold: item.productCount,
            price: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.productPrice),
            average: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.productAverage),
            total: new Intl.NumberFormat("pt-PT", {
              style: "currency",
              currency: "EUR",
            }).format(item.productTotal),
          });
        });

        response2.data.groupCount.map((item, index) => {
          productAmountChart.push({
            type: response2.data.groupName[index],
            value: item,
          });

          productSalesChart.push({
            type: response2.data.groupName[index],
            value: response2.data.groupTotal[index],
          });
        });

        setDataSource(products);
        setDataAmount(productAmountChart);
        setDataSales(productSalesChart);
      }
    })();
  }, []);

  var configAmount = {
    appendPadding: 10,
    data: dataAmount,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "spider",
      labelHeight: 28,
      content: "{name}",
    },
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
  };

  var configSales = {
    appendPadding: 10,
    data: dataSales,
    angleField: "value",
    colorField: "type",
    radius: 0.8,
    label: {
      type: "spider",
      labelHeight: 28,
      content: "{name}",
    },
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
  };

  const columns = [
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      align: "right",
      title: "Sold",
      dataIndex: "sold",
      key: "sold",
      sorter: (a, b) => a.sold - b.sold,
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Price",
      dataIndex: "price",
      key: "price",
      sorter: (a, b) =>
        parseFloat(a.price.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.price.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Average",
      dataIndex: "average",
      key: "average",
      sorter: (a, b) =>
        parseFloat(a.average.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.average.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Total",
      dataIndex: "total",
      key: "total",
      sorter: (a, b) =>
        parseFloat(a.total.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.total.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
  ];

  return (
    <div className="productPageContainer">
      <div className="fiscalYearContainer">
        <h2>Fiscal Year {fiscalYear}</h2>
      </div>
      <div className="graphsContainer">
        <Card title="Sales value by Product Group (€)" style={{ width: "45%" }}>
          {fiscalYear === null ? <h1>No Data</h1> : <Pie {...configSales} />}
        </Card>
        <Card
          title="Total Amount Sold by Product Group"
          style={{ width: "45%" }}
        >
          {fiscalYear === null ? <h1>No Data</h1> : <Pie {...configAmount} />}
        </Card>
      </div>
      <div className="tableContainer">
        <Table
          title={() => <h1>Products</h1>}
          dataSource={dataSource}
          columns={columns}
          bordered
          pagination={{ pageSize: 20 }}
          scroll={{ y: 240 }}
        />
      </div>
    </div>
  );
};

export default Product;

import React, { useEffect, useState } from "react";
import { Card } from "antd";

import api from "../../services/api";

import "./style.css";

const Company = () => {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [taxId, setTaxId] = useState("");
  const [saftVersion, setSaftVersion] = useState("");

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");

      if (year !== null) {
        const response = await api.get(`/api/company/${year}`);

        console.log(response.data);

        setName(response.data.CompanyName);
        setAddress(
          `${response.data.CompanyAddress.AddressDetail}, ${response.data.CompanyAddress.PostalCode} - ${response.data.CompanyAddress.City}, ${response.data.CompanyAddress.Region}, ${response.data.CompanyAddress.Country}`
        );
        setTaxId(response.data.ProductCompanyTaxID);
        setSaftVersion(response.data.AuditFileVersion);
      }
    })();
  }, []);

  return (
    <div className="companyContainer">
      <Card
        title="Information"
        style={{ width: "40%" }}
        className="companyCard"
      >
        <div className="companyCardContainer">
          <div className="companyTerms">
            <h4>Name:</h4>
            <h4>Address:</h4>
            <h4>Tax ID:</h4>
            <h4>SAF-T version:</h4>
          </div>
          <div className="companyData">
            <p>{name === "" ? "No Data" : name}</p>
            <p>{address === "" ? "No Data" : address}</p>
            <p>{taxId === "" ? "No Data" : taxId}</p>
            <p>{saftVersion === "" ? "No Data" : saftVersion}</p>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default Company;

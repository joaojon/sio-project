import React, { useEffect, useState } from "react";
import moment from "moment";

import { Button, Modal, Table } from "antd";

import api from "../../services/api";

const CustomerDetails = ({ supplierID }) => {
  const [dataSource, setDataSource] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [invoiceID, setInvoiceID] = useState("");

  const [dataSourceLines, setDataSourceLines] = useState([]);

  const showModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");

      const response = await api.get(
        `/api/buys-by-supplier/${supplierID}/${year}`
      );

      console.log(response.data);

      const sales = [];

      response.data.map((item, index) => {
        sales.push({
          key: index,
          invoiceId: item.id,
          date: moment(item.date).format("DD/MM/YYYY"),
          totalWithoutTax: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.totalWithoutTax),
          tax: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.tax),
          total: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.total),
        });
      });

      setDataSource(sales);
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const year = localStorage.getItem("fiscalYear");

      const invoiceIdBack = invoiceID.replace(" ", "%20");
      const invoiceIdComplete = invoiceIdBack.replace("/", "%2F");
      const response = await api.get(
        `/api/buys-lines/${invoiceIdComplete}/${year}`
      );

      console.log(response.data);

      const lines = [];

      response.data.map((item, index) => {
        lines.push({
          key: index,
          productCode: item.ProductCode,
          productDescription: item.ProductDescription,
          quantity: item.Quantity,
          unitPrice: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.UnitPrice),
          netTotal: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.NetTotal),
          grossTotal: new Intl.NumberFormat("pt-PT", {
            style: "currency",
            currency: "EUR",
          }).format(item.GrossTotal),
        });
      });

      setDataSourceLines(lines);
    })();
  }, [invoiceID]);

  const columns = [
    {
      title: "Invoice ID",
      dataIndex: "invoiceId",
      key: "invoiceId",
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
    },
    {
      align: "right",
      title: "Total Net Amount",
      dataIndex: "totalWithoutTax",
      key: "totalWithoutTax",
      sorter: (a, b) =>
        parseFloat(
          a.totalWithoutTax.split("€")[0].replace(" ", "").replace(",", ".")
        ) -
        parseFloat(
          b.totalWithoutTax.split("€")[0].replace(" ", "").replace(",", ".")
        ),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Tax Value",
      dataIndex: "tax",
      key: "tax",
      sorter: (a, b) =>
        parseFloat(a.tax.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.tax.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Total Gross Amount",
      dataIndex: "total",
      key: "total",
      sorter: (a, b) =>
        parseFloat(a.total.split("€")[0].replace(" ", "").replace(",", ".")) -
        parseFloat(b.total.split("€")[0].replace(" ", "").replace(",", ".")),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "center",
      title: "",
      dataIndex: "",
      key: "invoiceDetails",
      render: (_, record) => (
        <Button
          type="primary"
          onClick={() => {
            setInvoiceID(record.invoiceId);
            showModal();
          }}
        >
          Invoice Details
        </Button>
      ),
    },
  ];

  const columnsLines = [
    {
      title: "Product Code",
      dataIndex: "productCode",
      key: "productCode",
    },
    {
      title: "Product Description",
      dataIndex: "productDescription",
      key: "productDescription",
    },
    {
      align: "right",
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      align: "right",
      title: "Unit Price",
      dataIndex: "unitPrice",
      key: "unitPrice",
      sorter: (a, b) =>
        parseInt(a.unitPrice.split(" ")[0]) -
        parseInt(b.unitPrice.split(" ")[0]),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Net Total",
      dataIndex: "netTotal",
      key: "netTotal",
      sorter: (a, b) =>
        parseInt(a.netTotal.split(" ")[0]) - parseInt(b.netTotal.split(" ")[0]),
      sortDirections: ["descend", "ascend"],
    },
    {
      align: "right",
      title: "Gross Total",
      dataIndex: "grossTotal",
      key: "grossTotal",
      sorter: (a, b) =>
        parseInt(a.grossTotal.split(" ")[0]) -
        parseInt(b.grossTotal.split(" ")[0]),
      sortDirections: ["descend", "ascend"],
    },
  ];

  return (
    <div className="customerDetailsContainer">
      <Table
        title={() => <h1>Supplier {supplierID} Purchases</h1>}
        dataSource={dataSource}
        columns={columns}
        bordered
        pagination={{ pageSize: 20 }}
        scroll={{ y: 240 }}
        className="salesPeriodTable"
      />
      <Modal
        title={`Invoice ${invoiceID}`}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1200}
        style={{ top: 20 }}
      >
        <Table
          title={() => <h1>Invoice Lines</h1>}
          dataSource={dataSourceLines}
          columns={columnsLines}
          bordered
          pagination={{ pageSize: 20 }}
          scroll={{ y: 240 }}
        ></Table>
      </Modal>
    </div>
  );
};

export default CustomerDetails;

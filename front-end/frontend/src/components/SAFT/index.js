import React from "react";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { Card } from "antd";

import "./style.css";

const props = {
  name: "saft",
  action: "http://localhost:3000/api/upload",
  headers: {
    authorization: "authorization-text",
  },
  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

const SAFT = () => {
  return (
    <div className="SAFTContainer">
      <Card title="Import SAF-T" className="cardUpload">
        <Upload {...props}>
          <Button icon={<UploadOutlined />}>Click to Import</Button>
        </Upload>
      </Card>
    </div>
  );
};

export default SAFT;
